"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const allowedExt = [
    '.js',
    '.ico',
    '.css',
    '.png',
    '.jpg',
    '.woff2',
    '.woff',
    '.ttf',
    '.svg',
];
const resolvePath = (file) => path.resolve('..', '..', '..', '..', '..', 'vinilon-web', 'build', 'dist', `${file}`);
class FrontendMiddleware {
    use(req, res, next) {
        console.log('hahahha');
        const { url } = req;
        if (url.indexOf('/v1/api') === 1) {
            next();
        }
        else if (allowedExt.filter(ext => url.indexOf(ext) > 0).length > 0) {
            res.sendFile(resolvePath(url));
        }
        else {
            res.sendFile(resolvePath('index.html'));
        }
    }
}
exports.FrontendMiddleware = FrontendMiddleware;
