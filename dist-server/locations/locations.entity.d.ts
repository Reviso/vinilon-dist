import { DoEntity } from '../dos/dos.entity';
export declare class LocationsEntity {
    id: number;
    lat: number;
    lng: number;
    do: DoEntity;
}
