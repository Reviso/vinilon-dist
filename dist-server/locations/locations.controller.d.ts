import { LocationsEntity } from './locations.entity';
import { LocationsService } from './locations.service';
import { DosService } from '../dos/dos.service';
export declare class LocationsController {
    private service;
    private doService;
    constructor(service: LocationsService, doService: DosService);
    createOne(params: any, body: LocationsEntity): Promise<LocationsEntity>;
}
