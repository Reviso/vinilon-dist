import { RepositoryService } from '@nestjsx/crud/typeorm';
import { LocationsEntity } from './locations.entity';
export declare class LocationsService extends RepositoryService<LocationsEntity> {
    constructor(repo: any);
}
