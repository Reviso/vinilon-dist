"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const dos_entity_1 = require("../dos/dos.entity");
let LocationsEntity = class LocationsEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], LocationsEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({
        type: 'decimal',
        precision: 12,
        scale: 9,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], LocationsEntity.prototype, "lat", void 0);
__decorate([
    typeorm_1.Column({
        type: 'decimal',
        precision: 12,
        scale: 9,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], LocationsEntity.prototype, "lng", void 0);
__decorate([
    typeorm_1.ManyToOne(type => dos_entity_1.DoEntity, object => object.locations),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", dos_entity_1.DoEntity)
], LocationsEntity.prototype, "do", void 0);
LocationsEntity = __decorate([
    typeorm_1.Entity()
], LocationsEntity);
exports.LocationsEntity = LocationsEntity;
