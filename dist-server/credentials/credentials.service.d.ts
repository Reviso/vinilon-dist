import { CredentialEntity } from './credentials.entity';
import { RepositoryService } from '@nestjsx/crud/typeorm';
export declare class CredentialsService extends RepositoryService<CredentialEntity> {
    constructor(repo: any);
}
