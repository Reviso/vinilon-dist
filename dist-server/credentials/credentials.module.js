"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const credentials_service_1 = require("./credentials.service");
const credentials_controller_1 = require("./credentials.controller");
const typeorm_1 = require("@nestjs/typeorm");
const credentials_entity_1 = require("./credentials.entity");
const users_entity_1 = require("../users/users.entity");
const drivers_entity_1 = require("../drivers/drivers.entity");
const customers_entity_1 = require("../customers/customers.entity");
const users_service_1 = require("../users/users.service");
let CredentialsModule = class CredentialsModule {
};
CredentialsModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                credentials_entity_1.CredentialEntity,
                users_entity_1.UserEntity,
                drivers_entity_1.DriverEntity,
                customers_entity_1.CustomerEntity,
            ]),
        ],
        providers: [credentials_service_1.CredentialsService, users_service_1.UsersService],
        controllers: [credentials_controller_1.CredentialsController],
    })
], CredentialsModule);
exports.CredentialsModule = CredentialsModule;
