import { CredentialsService } from './credentials.service';
import { UsersService } from '../users/users.service';
export declare class CredentialsController {
    service: CredentialsService;
    private userService;
    constructor(service: CredentialsService, userService: UsersService);
}
