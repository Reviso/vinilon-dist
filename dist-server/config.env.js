"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config = {
    development: {
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'rootroot',
        database: 'vinilon',
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
        synchronize: true,
        logging: false,
    },
    production: {
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'dev',
        password: 'pass',
        database: 'vinilon',
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
        synchronize: true,
        logging: false,
    },
};
const envConfig = config[process.env.NODE_ENV || 'development'];
exports.default = envConfig;
