import { RepositoryService } from '@nestjsx/crud/typeorm';
import { VehicleEntity } from './vehicles.entity';
export declare class VehiclesService extends RepositoryService<VehicleEntity> {
    constructor(repo: any);
}
