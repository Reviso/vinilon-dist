"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const do_transactions_entity_1 = require("./do-transactions.entity");
const do_transactions_service_1 = require("./do-transactions.service");
const dos_service_1 = require("../dos/dos.service");
const do_status_entity_1 = require("../dos/do-status.entity");
const vehicles_service_1 = require("../vehicles/vehicles.service");
const acl_service_1 = require("../acl/acl.service");
const blast_email_service_1 = require("../blast-email/blast-email.service");
const passport_1 = require("@nestjs/passport");
const admin = require("firebase-admin");
let DoTransactionsController = class DoTransactionsController {
    constructor(service, doService, vehicleService, blastEmailService) {
        this.service = service;
        this.doService = doService;
        this.vehicleService = vehicleService;
        this.blastEmailService = blastEmailService;
    }
    createOne(params, body) {
        body.do.status = body.status;
        body.do.currentDriver = body.assignedDriver;
        return this.vehicleService
            .find({
            relations: ['driver'],
        })
            .then(vehicles => {
            body.do.vehicle = vehicles.find(x => x.driver && body.assignedDriver
                ? x.driver.id === body.assignedDriver.id
                : false);
            if (!body.do.historyDrivers) {
                body.do.historyDrivers = [];
            }
            body.do.historyDrivers.push(body.assignedDriver);
            body.do.updatedDate = new Date();
            return this.doService.updateOne(body.do).then(next => {
                body.do = next;
                return this.service.createOne(body, []).then(next => {
                    const title = 'DO ' + next.do.noDO + ' diberikan kepada anda';
                    this.sendNotification(next.assignedDriver.firebaseToken, title, '');
                });
            });
        });
    }
    driverMutation(req, doid, body) {
        return this.doService.findOne({
            where: {
                id: parseInt(doid)
            },
        }).then((deliveryOrder) => __awaiter(this, void 0, void 0, function* () {
            let dataOrder = yield this.doService.find({
                where: {
                    noDO: deliveryOrder.noDO,
                    company: deliveryOrder.company
                }
            });
            let i = 0;
            for (i = 0; i <= dataOrder.length - 1; i++) {
                if (i == dataOrder.length - 1) {
                    return this.doService.findOne(dataOrder[i].id).then(deliveryOrder => {
                        deliveryOrder.status = do_status_entity_1.DoStatus.Mutated;
                        return this.doService.updateOne(deliveryOrder).then(next => {
                            body['do'] = next;
                            body['status'] = do_status_entity_1.DoStatus.Mutated;
                            body['mutationCode'] = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
                            body['assignedDriver'] = req.user;
                            return this.service.createOne(body, []).then(transaction => {
                                deliveryOrder.currentTransactionId = transaction.id;
                                return this.doService.updateOne(deliveryOrder).then(next => {
                                    return this.sendEmail('mutate', next).then(email => {
                                        return transaction;
                                    });
                                });
                            });
                        });
                    });
                }
                else {
                    this.doService.findOne(dataOrder[i].id).then(deliveryOrder => {
                        deliveryOrder.status = do_status_entity_1.DoStatus.Mutated;
                        this.doService.updateOne(deliveryOrder).then(next => {
                            body['do'] = next;
                            body['status'] = do_status_entity_1.DoStatus.Mutated;
                            body['mutationCode'] = (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
                            body['assignedDriver'] = req.user;
                            this.service.createOne(body, []).then(transaction => {
                                deliveryOrder.currentTransactionId = transaction.id;
                                this.doService.updateOne(deliveryOrder);
                            });
                        });
                    });
                }
            }
        }));
    }
    sendNotification(token, title, body) {
        if (token == null || token == undefined || token == '') {
            return;
        }
        admin
            .messaging()
            .sendToDevice(token, {
            data: {},
            notification: {
                title,
                body,
            },
        })
            .then(success => {
            success.results.forEach(result => {
                console.log(result);
            });
            console.log(success);
        }, error => {
            console.log(error);
        });
    }
    sendEmail(actionCode, deliveryOrder) {
        return this.blastEmailService.sendMailToDo(deliveryOrder.id, actionCode);
    }
};
__decorate([
    crud_1.Override(),
    __param(0, crud_1.ParsedParams()), __param(1, crud_1.ParsedBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, do_transactions_entity_1.DoTransactionEntity]),
    __metadata("design:returntype", void 0)
], DoTransactionsController.prototype, "createOne", null);
__decorate([
    common_1.Post('mutate'),
    common_1.UseGuards(passport_1.AuthGuard()),
    __param(0, common_1.Req()),
    __param(1, common_1.Query('doId')),
    __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, Object]),
    __metadata("design:returntype", void 0)
], DoTransactionsController.prototype, "driverMutation", null);
DoTransactionsController = __decorate([
    common_1.UseGuards(acl_service_1.ACLGuard),
    crud_1.Feature('DoTransactions'),
    crud_1.Crud(do_transactions_entity_1.DoTransactionEntity, {
        options: {
            join: {
                assignedDriver: {
                    allow: [],
                },
                do: {
                    allow: [],
                },
                medias: {
                    allow: [],
                },
                problem: {
                    allow: [],
                },
            },
        },
    }),
    common_1.Controller('doTransactions'),
    __metadata("design:paramtypes", [do_transactions_service_1.DoTransactionsService,
        dos_service_1.DosService,
        vehicles_service_1.VehiclesService,
        blast_email_service_1.BlastEmailService])
], DoTransactionsController);
exports.DoTransactionsController = DoTransactionsController;
