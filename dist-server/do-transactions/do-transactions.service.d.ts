import { RepositoryService } from '@nestjsx/crud/typeorm';
import { DoTransactionEntity } from './do-transactions.entity';
export declare class DoTransactionsService extends RepositoryService<DoTransactionEntity> {
    constructor(repo: any);
}
