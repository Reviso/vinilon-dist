import { DriverEntity } from '../drivers/drivers.entity';
import { MediaEntity } from '../medias/medias.entity';
import { DoEntity } from '../dos/dos.entity';
export declare class DoTransactionEntity {
    id?: number;
    status?: string;
    assignedDriver?: DriverEntity;
    medias?: MediaEntity[];
    do?: DoEntity;
    date?: Date;
    problem?: string;
    notes?: string;
    lastPosition?: string;
    mutationCode?: string;
}
