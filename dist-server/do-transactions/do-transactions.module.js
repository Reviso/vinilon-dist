"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const do_transactions_entity_1 = require("./do-transactions.entity");
const do_transactions_controller_1 = require("./do-transactions.controller");
const do_transactions_service_1 = require("./do-transactions.service");
const dos_entity_1 = require("../dos/dos.entity");
const dos_service_1 = require("../dos/dos.service");
const vehicles_service_1 = require("../vehicles/vehicles.service");
const vehicles_entity_1 = require("../vehicles/vehicles.entity");
const blast_email_service_1 = require("../blast-email/blast-email.service");
const blast_email_entity_1 = require("../blast-email/blast-email.entity");
const passport_1 = require("@nestjs/passport");
let DoTransactionsModule = class DoTransactionsModule {
};
DoTransactionsModule = __decorate([
    common_1.Module({
        imports: [
            passport_1.PassportModule.register({ defaultStrategy: 'jwt' }),
            typeorm_1.TypeOrmModule.forFeature([
                do_transactions_entity_1.DoTransactionEntity,
                dos_entity_1.DoEntity,
                vehicles_entity_1.VehicleEntity,
                blast_email_entity_1.BlastEmailEntity,
            ]),
        ],
        providers: [
            do_transactions_service_1.DoTransactionsService,
            dos_service_1.DosService,
            vehicles_service_1.VehiclesService,
            blast_email_service_1.BlastEmailService,
        ],
        controllers: [do_transactions_controller_1.DoTransactionsController],
    })
], DoTransactionsModule);
exports.DoTransactionsModule = DoTransactionsModule;
