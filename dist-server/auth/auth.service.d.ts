import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { CredentialsService } from '../credentials/credentials.service';
import { CredentialEntity } from '../credentials/credentials.entity';
import { DriversService } from '../drivers/drivers.service';
import { UsersService } from '../users/users.service';
import { CustomersService } from '../customers/customers.service';
export declare class AuthService {
    private readonly jwtService;
    private credentialService;
    private driverService;
    private customersService;
    private usersService;
    constructor(jwtService: JwtService, credentialService: CredentialsService, driverService: DriversService, customersService: CustomersService, usersService: UsersService);
    createToken(user: JwtPayload, type: string): Promise<{
        expiresIn: number;
        accessToken: string;
    }>;
    validateUser(payload: CredentialEntity): Promise<any>;
    customer(): void;
    login(user: JwtPayload, type: string): Promise<any>;
}
