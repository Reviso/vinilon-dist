"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const dos_get_from_server_service_1 = require("./dos/dos-get-from-server.service");
const companies_entity_1 = require("./companies/companies.entity");
const config_1 = require("./config");
function bootstrap() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = yield core_1.NestFactory.create(app_module_1.AppModule);
        const service = app.get(dos_get_from_server_service_1.GetDoFromServerService);
        const companyNameAndCode = [
            { code: '288', name: 'PT RUSLI VINILON SAKTI' },
            { code: '888', name: 'PT SINARMAS ANDHIKA' },
            { code: '555', name: 'PT VINILON JAYA SAKTI' },
            { code: '777A', name: 'PT FLOW SOLUTION INDONESIA cb Cikarang' },
            { code: '777B', name: 'PT FLOW SOLUTION INDONESIA cb Sidoarjo' },
            { code: '777C', name: 'PT FLOW SOLUTION INDONESIA cb Medan' },
            { code: '882', name: 'PT TIRTA PRATAMA METERINDO' },
            { code: '881', name: 'PT RITEK PLASINDO' },
            { code: '771A', name: 'PT RITEL JAYA SAKTI cb Cikarang' },
            { code: '771B', name: 'PT RITEL JAYA SAKTI cb Bitung' },
            { code: '771C', name: 'PT RITEL JAYA SAKTI cb Cileungsi' },
        ];
        const companies = companyNameAndCode.map(n => {
            const company = new companies_entity_1.CompanyEntity();
            company.code = n.code;
            company.name = n.name;
            return company;
        });
        setInterval(() => {
            service.companyService.find().then(savedCompanies => {
                const combinedCompanies = savedCompanies;
                if (combinedCompanies.length > 0) {
                    service.getData(combinedCompanies, 0);
                    console.log(combinedCompanies);
                }
            });
        }, config_1.config.jobsDuration);
        yield app.listen(config_1.config.jobsPort);
    });
}
bootstrap();
