"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const problems_entity_1 = require("../problems/problems.entity");
const dos_entity_1 = require("../dos/dos.entity");
let DoMutationsEntity = class DoMutationsEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], DoMutationsEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => problems_entity_1.ProblemEntity, object => object.mutation),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", problems_entity_1.ProblemEntity)
], DoMutationsEntity.prototype, "problem", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoMutationsEntity.prototype, "notes", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoMutationsEntity.prototype, "lastPosition", void 0);
__decorate([
    typeorm_1.ManyToOne(type => dos_entity_1.DoEntity, object => object.address1),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", dos_entity_1.DoEntity)
], DoMutationsEntity.prototype, "do", void 0);
DoMutationsEntity = __decorate([
    typeorm_1.Entity()
], DoMutationsEntity);
exports.DoMutationsEntity = DoMutationsEntity;
