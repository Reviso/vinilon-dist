import { DoMutationsService } from './do-mutations.service';
import { CrudController } from '@nestjsx/crud';
import { DoMutationsEntity } from './do-mutations.entity';
export declare class DoMutationsController {
    service: DoMutationsService;
    constructor(service: DoMutationsService);
    readonly base: CrudController<DoMutationsService, DoMutationsEntity>;
    createOne(params: any, body: DoMutationsEntity): Promise<DoMutationsEntity>;
}
