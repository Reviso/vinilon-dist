import { ProblemEntity } from '../problems/problems.entity';
import { DoEntity } from '../dos/dos.entity';
export declare class DoMutationsEntity {
    id: number;
    problem: ProblemEntity;
    notes: string;
    lastPosition: string;
    do: DoEntity;
}
