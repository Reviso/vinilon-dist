import { CustomerEntity } from './customers.entity';
import { CrudController } from '@nestjsx/crud';
import { CustomersService } from './customers.service';
import { CredentialsService } from '../credentials/credentials.service';
import { CredentialEntity } from 'src/credentials/credentials.entity';
export declare class CustomersController {
    service: CustomersService;
    credentialsService: CredentialsService;
    constructor(service: CustomersService, credentialsService: CredentialsService);
    readonly base: CrudController<CustomersService, CustomerEntity>;
    createOne(params: any, body: CustomerEntity): Promise<CustomerEntity>;
    createOrUpdateCredential(credential: CredentialEntity, listener: (credential: CredentialEntity) => void): Promise<void>;
}
