"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const customers_entity_1 = require("./customers.entity");
const crud_1 = require("@nestjsx/crud");
const customers_service_1 = require("./customers.service");
const credentials_service_1 = require("../credentials/credentials.service");
const acl_service_1 = require("../acl/acl.service");
const passport_1 = require("@nestjs/passport");
let CustomersController = class CustomersController {
    constructor(service, credentialsService) {
        this.service = service;
        this.credentialsService = credentialsService;
    }
    get base() {
        return this;
    }
    createOne(params, body) {
        body.credential.email = body.email;
        return this.credentialsService.createOne(body.credential, []).then(next => {
            body.credential = next;
            return this.service.createOne(body, []);
        });
    }
    createOrUpdateCredential(credential, listener) {
        return this.credentialsService
            .findOne(null, {
            where: {
                email: credential.email,
                password: credential.password
            },
        })
            .then(savedCredential => {
            if (savedCredential) {
                listener(savedCredential);
            }
            else {
                return this.credentialsService.createOne(credential, []).then(saved => {
                    listener(saved);
                }, error => {
                    console.log(error);
                    listener(null);
                });
            }
        });
    }
};
__decorate([
    crud_1.Override(),
    __param(0, crud_1.ParsedParams()), __param(1, crud_1.ParsedBody()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, customers_entity_1.CustomerEntity]),
    __metadata("design:returntype", void 0)
], CustomersController.prototype, "createOne", null);
CustomersController = __decorate([
    common_1.UseGuards(passport_1.AuthGuard(), acl_service_1.ACLGuard),
    crud_1.Feature('Customers'),
    crud_1.Crud(customers_entity_1.CustomerEntity, {
        options: {
            join: {
                credential: {
                    allow: [],
                },
            },
        },
    }),
    common_1.Controller('customers'),
    __metadata("design:paramtypes", [customers_service_1.CustomersService,
        credentials_service_1.CredentialsService])
], CustomersController);
exports.CustomersController = CustomersController;
