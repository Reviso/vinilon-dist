import { CredentialEntity } from '../credentials/credentials.entity';
import { DoEntity } from '../dos/dos.entity';
export declare class CustomerEntity {
    id: number;
    code: string;
    name: string;
    address: string;
    phone: string;
    email: string;
    firebaseToken: string;
    token: string;
    isDelete: boolean;
    credential: CredentialEntity;
    dos: DoEntity[];
}
