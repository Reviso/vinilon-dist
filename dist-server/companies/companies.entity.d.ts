import { UserEntity } from '../users/users.entity';
export declare class CompanyEntity {
    id: number;
    name: string;
    code: string;
    employees: UserEntity[];
}
