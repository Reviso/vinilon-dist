"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const crud_1 = require("@nestjsx/crud");
const companies_service_1 = require("./companies.service");
const companies_entity_1 = require("./companies.entity");
const acl_service_1 = require("../acl/acl.service");
const passport_1 = require("@nestjs/passport");
let CompaniesController = class CompaniesController {
    constructor(service) {
        this.service = service;
    }
};
CompaniesController = __decorate([
    common_1.UseGuards(passport_1.AuthGuard(), acl_service_1.ACLGuard),
    crud_1.Feature('Companies'),
    crud_1.Crud(companies_entity_1.CompanyEntity),
    common_1.Controller('companies'),
    __metadata("design:paramtypes", [companies_service_1.CompaniesService])
], CompaniesController);
exports.CompaniesController = CompaniesController;
