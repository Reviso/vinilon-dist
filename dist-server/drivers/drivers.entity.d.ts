import { DoTransactionEntity } from '../do-transactions/do-transactions.entity';
import { DoEntity } from '../dos/dos.entity';
import { CredentialEntity } from '../credentials/credentials.entity';
export declare class DriverEntity {
    id: number;
    drivingLicenseNo: string;
    name: string;
    address: string;
    phone: string;
    email: string;
    firebaseToken: string;
    notes: string;
    token: string;
    isDelete: boolean;
    credential: CredentialEntity;
    transactions: DoTransactionEntity[];
    dos: DoEntity[];
    allDos: DoEntity[];
}
