"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const swagger_1 = require("@nestjs/swagger");
const do_transactions_entity_1 = require("../do-transactions/do-transactions.entity");
const dos_entity_1 = require("../dos/dos.entity");
const credentials_entity_1 = require("../credentials/credentials.entity");
let DriverEntity = class DriverEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], DriverEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DriverEntity.prototype, "drivingLicenseNo", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], DriverEntity.prototype, "name", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], DriverEntity.prototype, "address", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({
        unique: true,
    }),
    __metadata("design:type", String)
], DriverEntity.prototype, "phone", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], DriverEntity.prototype, "email", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({
        nullable: true,
    }),
    __metadata("design:type", String)
], DriverEntity.prototype, "firebaseToken", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DriverEntity.prototype, "notes", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], DriverEntity.prototype, "token", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true, default: false }),
    __metadata("design:type", Boolean)
], DriverEntity.prototype, "isDelete", void 0);
__decorate([
    typeorm_1.OneToOne(type => credentials_entity_1.CredentialEntity, { onDelete: 'CASCADE' }),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", credentials_entity_1.CredentialEntity)
], DriverEntity.prototype, "credential", void 0);
__decorate([
    typeorm_1.OneToMany(type => do_transactions_entity_1.DoTransactionEntity, object => object.assignedDriver, {
        onDelete: 'SET NULL',
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Array)
], DriverEntity.prototype, "transactions", void 0);
__decorate([
    typeorm_1.ManyToOne(type => dos_entity_1.DoEntity, object => object.currentDriver, {
        onDelete: 'SET NULL',
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Array)
], DriverEntity.prototype, "dos", void 0);
__decorate([
    typeorm_1.ManyToMany(type => dos_entity_1.DoEntity, object => object.historyDrivers),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Array)
], DriverEntity.prototype, "allDos", void 0);
DriverEntity = __decorate([
    typeorm_1.Entity()
], DriverEntity);
exports.DriverEntity = DriverEntity;
