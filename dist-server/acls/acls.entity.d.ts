import { UserEntity } from '../users/users.entity';
export declare class AclsEntity {
    id: number;
    moduleName: string;
    manager: boolean;
    operator: boolean;
    userId: number;
    user: UserEntity;
}
