import { RepositoryService } from '@nestjsx/crud/typeorm';
import { ProblemEntity } from './problems.entity';
export declare class ProblemsService extends RepositoryService<ProblemEntity> {
    constructor(repo: any);
}
