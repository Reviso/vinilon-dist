import { DoTransactionEntity } from '../do-transactions/do-transactions.entity';
import { BlastEmailEntity } from '../blast-email/blast-email.entity';
export declare class MediaEntity {
    id: number;
    url: string;
    transaction: DoTransactionEntity;
    email: BlastEmailEntity;
}
