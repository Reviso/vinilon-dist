"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const crud_1 = require("@nestjsx/crud");
const medias_service_1 = require("./medias.service");
const medias_entity_1 = require("./medias.entity");
let MediaController = class MediaController {
    constructor(service) {
        this.service = service;
    }
    uploadMedia(files) {
        const medias = files.files.map(file => {
            const media = new medias_entity_1.MediaEntity();
            media.url = file.path;
            return media;
        });
        return this.service.createMany({ bulk: medias });
    }
};
__decorate([
    common_1.Post('upload'),
    common_1.UseInterceptors(platform_express_1.FileFieldsInterceptor([
        {
            name: 'files',
        },
    ])),
    __param(0, common_1.UploadedFiles()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], MediaController.prototype, "uploadMedia", null);
MediaController = __decorate([
    crud_1.Crud(medias_entity_1.MediaEntity, {
        options: {
            join: {
                transaction: {
                    allow: [],
                },
            },
        },
    }),
    crud_1.Feature('Medias'),
    common_1.Controller('medias'),
    __metadata("design:paramtypes", [medias_service_1.MediaService])
], MediaController);
exports.MediaController = MediaController;
