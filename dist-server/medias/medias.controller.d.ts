import { MediaService } from './medias.service';
import { MediaEntity } from './medias.entity';
export declare class MediaController {
    service: MediaService;
    constructor(service: MediaService);
    uploadMedia(files: any): Promise<MediaEntity[]>;
}
