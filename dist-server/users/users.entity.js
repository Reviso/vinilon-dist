"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const companies_entity_1 = require("../companies/companies.entity");
const swagger_1 = require("@nestjs/swagger");
const credentials_entity_1 = require("../credentials/credentials.entity");
const acls_entity_1 = require("../acls/acls.entity");
const user_type_entity_1 = require("./user-type.entity");
let UserEntity = class UserEntity {
};
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], UserEntity.prototype, "id", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], UserEntity.prototype, "name", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "address", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "phone", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "email", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], UserEntity.prototype, "companyId", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.ManyToOne(type => companies_entity_1.CompanyEntity, company => company.employees, {
        onDelete: 'SET NULL',
    }),
    __metadata("design:type", companies_entity_1.CompanyEntity)
], UserEntity.prototype, "company", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "department", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "token", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true, default: false }),
    __metadata("design:type", Boolean)
], UserEntity.prototype, "isDelete", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    typeorm_1.Column({ nullable: true, default: user_type_entity_1.UserLevel.Admin }),
    __metadata("design:type", String)
], UserEntity.prototype, "level", void 0);
__decorate([
    typeorm_1.OneToOne(type => credentials_entity_1.CredentialEntity, { onDelete: 'CASCADE' }),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", credentials_entity_1.CredentialEntity)
], UserEntity.prototype, "credential", void 0);
__decorate([
    typeorm_1.OneToMany(type => acls_entity_1.AclsEntity, transaction => transaction.user, {
        onDelete: 'CASCADE',
    }),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Array)
], UserEntity.prototype, "acls", void 0);
UserEntity = __decorate([
    typeorm_1.Entity()
], UserEntity);
exports.UserEntity = UserEntity;
