"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const dos_service_1 = require("./dos.service");
const medias_entity_1 = require("../medias/medias.entity");
const do_status_entity_1 = require("./do-status.entity");
const customers_entity_1 = require("../customers/customers.entity");
const do_transactions_service_1 = require("../do-transactions/do-transactions.service");
const medias_service_1 = require("../medias/medias.service");
const credentials_service_1 = require("../credentials/credentials.service");
const customers_service_1 = require("../customers/customers.service");
const companies_service_1 = require("../companies/companies.service");
const blast_email_service_1 = require("../blast-email/blast-email.service");
const platform_express_1 = require("@nestjs/platform-express");
const drivers_entity_1 = require("../drivers/drivers.entity");
const do_transactions_entity_1 = require("../do-transactions/do-transactions.entity");
const admin = require("firebase-admin");
const dos_entity_1 = require("./dos.entity");
const crud_1 = require("@nestjsx/crud");
let DosNoAuthController = class DosNoAuthController {
    constructor(service, transactionService, mediaService, credentialService, customerService, companyService, httpService, blastEmailService) {
        this.service = service;
        this.transactionService = transactionService;
        this.mediaService = mediaService;
        this.credentialService = credentialService;
        this.customerService = customerService;
        this.companyService = companyService;
        this.httpService = httpService;
        this.blastEmailService = blastEmailService;
    }
    get base() {
        return this;
    }
    getOne(req, parsedQuery, parsedOptions) {
        return this.base.getOneBase(parsedQuery, parsedOptions).then(next => {
            const transactionIds = next.transactions.map(t => {
                return { id: t.id };
            });
            if (transactionIds.length > 0) {
                const m = {
                    where: transactionIds,
                    join: {
                        alias: 'transaction',
                        leftJoinAndSelect: {
                            medias: 'transaction.medias',
                        },
                    },
                };
                return this.transactionService.find(m).then(transactions => {
                    next.transactions = transactions;
                    return next;
                });
            }
            else {
                return next;
            }
        });
    }
    getMany(req, parsedQuery, parsedOptions) {
        return __awaiter(this, void 0, void 0, function* () {
            if (parsedQuery.filter) {
                if (req.user) {
                    if (req.user instanceof drivers_entity_1.DriverEntity) {
                        if (parsedQuery.join.find(x => x.field == 'historyDrivers')) {
                            parsedQuery.filter.push({
                                field: 'historyDrivers.id',
                                operator: 'eq',
                                value: req.user.id,
                            });
                        }
                        else {
                            parsedQuery.filter.push({
                                field: 'currentDriver.id',
                                operator: 'eq',
                                value: req.user.id,
                            });
                            parsedQuery.join.push({
                                field: 'currentDriver',
                            });
                        }
                    }
                    else if (req.user instanceof customers_entity_1.CustomerEntity) {
                        parsedQuery.filter.push({
                            field: 'customer.id',
                            operator: 'eq',
                            value: req.user.id,
                        });
                        parsedQuery.join.push({
                            field: 'customer',
                        });
                    }
                }
            }
            let dataDos = yield this.base.getManyBase(parsedQuery, parsedOptions);
            if (!parsedQuery.page) {
                let data = this.removeDuplicates(dataDos, "noDO", "company");
                return new Promise((resolve, reject) => {
                    resolve(data);
                });
            }
            else {
                return new Promise((resolve, reject) => {
                    resolve(dataDos);
                });
            }
        });
    }
    removeDuplicates(array, key, key2) {
        let lookup = {};
        let result = [];
        var counter;
        counter = 0;
        var keyArray;
        array.forEach(element => {
            keyArray = String(element[key]) + "_" + element[key2];
            if ((!lookup[element[key] + "_" + element[key2]])) {
                lookup[element[key] + "_" + element[key2]] = true;
                result.push(element);
                counter = counter + 1;
            }
        });
        return result;
    }
    loadInDo(req, files, doId, deviceId) {
        const medias = this.createMediaEntitiesFrom(files.loadInPhotos);
        return this.createDoTransactionWith(doId, deviceId, medias, do_status_entity_1.DoStatus.LoadIn);
    }
    sendDo(req, files, doId, deviceId) {
        const medias = this.createMediaEntitiesFrom(files.letterPhotos);
        return this.createDoTransactionWith(doId, deviceId, medias, do_status_entity_1.DoStatus.Started).then(next => {
            return this.sendEmail('send', parseInt(doId)).then(email => {
                return next;
            }, error => {
                return next;
            });
        });
    }
    finishDo(req, files, doId, deviceId) {
        const medias = this.createMediaEntitiesFrom(files.loadOutPhotos);
        const letterMedias = this.createMediaEntitiesFrom(files.letterPhotos);
        return this.createDoTransactionWith(doId, deviceId, letterMedias, do_status_entity_1.DoStatus.SignedDeliveryOrder).then(next => {
            return this.createDoTransactionWith(doId, deviceId, medias, do_status_entity_1.DoStatus.Finished).then(next => {
                return this.sendEmail('finish', parseInt(doId)).then(email => {
                    return next;
                }, error => {
                    return next;
                });
            });
        });
    }
    createDoTransactionWith(doId, deviceId, files, status) {
        return this.service
            .findOne({
            where: {
                id: parseInt(doId),
            },
            relations: ['customer'],
        }).then((deliveryOrder) => __awaiter(this, void 0, void 0, function* () {
            if (!deliveryOrder) {
                throw new common_1.BadRequestException();
            }
            let dataOrder = yield this.service.find({
                where: {
                    noDO: deliveryOrder.noDO,
                    company: deliveryOrder.company
                }
            });
            dataOrder.forEach(itemDeliveryOrder => {
                if (itemDeliveryOrder.customer && itemDeliveryOrder.customer.firebaseToken) {
                    this.sendDoStatusNotification(itemDeliveryOrder.customer, status, itemDeliveryOrder);
                }
                itemDeliveryOrder.deviceId = deviceId;
                itemDeliveryOrder.status = status;
                itemDeliveryOrder.updatedDate = new Date();
                return this.service
                    .updateOne(itemDeliveryOrder)
                    .then(savedDeliveryOrdeer => {
                    return this.mediaService
                        .createMany({
                        bulk: files,
                    })
                        .then(next => {
                        const transaction = new do_transactions_entity_1.DoTransactionEntity();
                        transaction.status = status;
                        transaction.do = savedDeliveryOrdeer;
                        transaction.medias = next;
                        return this.transactionService
                            .createOne(transaction, [])
                            .then(savedTransaction => {
                            itemDeliveryOrder.currentTransactionId = savedTransaction.id;
                            return this.service.updateOne(itemDeliveryOrder);
                        });
                    });
                });
            });
            return this.service.updateOne(dataOrder[0]).then(savedDeliveryOrdeer => {
                return this.mediaService.createMany({
                    bulk: files,
                }).then(next => {
                    const transaction = new do_transactions_entity_1.DoTransactionEntity();
                    transaction.status = status;
                    transaction.do = savedDeliveryOrdeer;
                    transaction.medias = next;
                    return this.transactionService.createOne(transaction, [])
                        .then(savedTransaction => {
                        dataOrder[0].currentTransactionId = savedTransaction.id;
                        return this.service.updateOne(dataOrder[0]);
                    });
                });
            });
        }));
    }
    createDoTransactionWithData(doId, driver, files, status) {
        return this.service.findOne({
            where: {
                id: parseInt(doId),
                currentDriver: driver,
            },
            relations: ['customer'],
        }).then((deliveryOrder) => __awaiter(this, void 0, void 0, function* () {
            if (!deliveryOrder) {
                throw new common_1.BadRequestException();
            }
            let dataOrder = yield this.service.find({
                where: {
                    noDO: deliveryOrder.noDO,
                    company: deliveryOrder.company
                }
            });
            dataOrder.forEach(itemDeliveryOrder => {
                if (itemDeliveryOrder.customer && itemDeliveryOrder.customer.firebaseToken) {
                    this.sendDoStatusNotification(itemDeliveryOrder.customer, status, itemDeliveryOrder);
                }
                itemDeliveryOrder.status = status;
                itemDeliveryOrder.updatedDate = new Date();
                this.service.updateOne(itemDeliveryOrder).then(savedDeliveryOrdeer => {
                    this.mediaService.createMany({
                        bulk: files,
                    }).then(next => {
                        const transaction = new do_transactions_entity_1.DoTransactionEntity();
                        transaction.assignedDriver = driver;
                        transaction.status = status;
                        transaction.do = savedDeliveryOrdeer;
                        transaction.medias = next;
                        this.transactionService.createOne(transaction, [])
                            .then(savedTransaction => {
                            itemDeliveryOrder.currentTransactionId = savedTransaction.id;
                            this.service.updateOne(itemDeliveryOrder);
                        });
                    });
                });
            });
            return this.service.updateOne(dataOrder[0]).then(savedDeliveryOrdeer => {
                return this.mediaService.createMany({
                    bulk: files,
                }).then(next => {
                    const transaction = new do_transactions_entity_1.DoTransactionEntity();
                    transaction.assignedDriver = driver;
                    transaction.status = status;
                    transaction.do = savedDeliveryOrdeer;
                    transaction.medias = next;
                    return this.transactionService.createOne(transaction, [])
                        .then(savedTransaction => {
                        dataOrder[0].currentTransactionId = savedTransaction.id;
                        return this.service.updateOne(dataOrder[0]);
                    });
                });
            });
        }));
    }
    sendDoStatusNotification(customer, doStatus, deliveryOrder) {
        let title = '';
        switch (doStatus) {
            case do_status_entity_1.DoStatus.LoadIn:
                title = 'Pesanan anda (' + deliveryOrder.noDO + ') sudah di load in';
                break;
            case do_status_entity_1.DoStatus.Started:
                title = 'Pesanan anda (' + deliveryOrder.noDO + ') sedang dikirim';
                break;
            case do_status_entity_1.DoStatus.Finished:
                title =
                    'Pesanan anda (' + deliveryOrder.noDO + ') sudah sampai di tujuan';
                break;
        }
        this.sendNotification(customer.firebaseToken, title, '');
    }
    sendNotification(token, title, body) {
        admin
            .messaging()
            .sendToDevice(token, {
            data: {},
            notification: {
                title,
                body,
            },
        })
            .then(success => {
            success.results.forEach(result => {
                console.log(result);
            });
            console.log(success);
        }, error => {
            console.log(error);
        });
    }
    createMediaEntitiesFrom(files) {
        return files.map(file => {
            const media = new medias_entity_1.MediaEntity();
            media.url = file.path.replace('\\', '/');
            return media;
        });
    }
    sendEmail(actionCode, doId) {
        return this.blastEmailService.sendMailToDo(doId, actionCode);
    }
};
__decorate([
    crud_1.Override(),
    __param(0, common_1.Req()), __param(1, crud_1.ParsedQuery()), __param(2, crud_1.ParsedOptions()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, crud_1.RestfulParamsDto, Object]),
    __metadata("design:returntype", void 0)
], DosNoAuthController.prototype, "getOne", null);
__decorate([
    crud_1.Override(),
    __param(0, common_1.Req()), __param(1, crud_1.ParsedQuery()), __param(2, crud_1.ParsedOptions()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, crud_1.RestfulParamsDto, Object]),
    __metadata("design:returntype", Promise)
], DosNoAuthController.prototype, "getMany", null);
__decorate([
    common_1.Post('loadIn'),
    common_1.UseInterceptors(platform_express_1.FileFieldsInterceptor([
        {
            name: 'loadInPhotos',
        },
    ])),
    __param(0, common_1.Req()),
    __param(1, common_1.UploadedFiles()),
    __param(2, common_1.Query('doId')),
    __param(3, common_1.Query('deviceId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String, String]),
    __metadata("design:returntype", void 0)
], DosNoAuthController.prototype, "loadInDo", null);
__decorate([
    common_1.Post('send'),
    common_1.UseInterceptors(platform_express_1.FileFieldsInterceptor([
        {
            name: 'letterPhotos',
        },
    ])),
    __param(0, common_1.Req()),
    __param(1, common_1.UploadedFiles()),
    __param(2, common_1.Query('doId')),
    __param(3, common_1.Query('deviceId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String, String]),
    __metadata("design:returntype", void 0)
], DosNoAuthController.prototype, "sendDo", null);
__decorate([
    common_1.Post('finish'),
    common_1.UseInterceptors(platform_express_1.FileFieldsInterceptor([
        {
            name: 'loadOutPhotos',
        },
        {
            name: 'letterPhotos',
        },
    ])),
    __param(0, common_1.Req()),
    __param(1, common_1.UploadedFiles()),
    __param(2, common_1.Query('doId')),
    __param(3, common_1.Query('deviceId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String, String]),
    __metadata("design:returntype", void 0)
], DosNoAuthController.prototype, "finishDo", null);
DosNoAuthController = __decorate([
    crud_1.Crud(dos_entity_1.DoEntity, {
        options: {
            join: {
                transactions: {
                    allow: [],
                },
                currentDriver: {
                    allow: [],
                },
                locations: {
                    allow: [],
                },
                customer: {
                    allow: [],
                },
                vehicle: {
                    allow: [],
                },
            },
        },
    }),
    common_1.Controller('dos-no-auth'),
    __metadata("design:paramtypes", [dos_service_1.DosService,
        do_transactions_service_1.DoTransactionsService,
        medias_service_1.MediaService,
        credentials_service_1.CredentialsService,
        customers_service_1.CustomersService,
        companies_service_1.CompaniesService,
        common_1.HttpService,
        blast_email_service_1.BlastEmailService])
], DosNoAuthController);
exports.DosNoAuthController = DosNoAuthController;
