import { HttpService } from '@nestjs/common';
import { DosService } from './dos.service';
import { CredentialsService } from '../credentials/credentials.service';
import { CustomersService } from '../customers/customers.service';
import { CompaniesService } from '../companies/companies.service';
import { CompanyEntity } from '../companies/companies.entity';
import { CustomerEntity } from '../customers/customers.entity';
import { CredentialEntity } from '../credentials/credentials.entity';
import { DoEntity } from './dos.entity';
export declare class GetDoFromServerService {
    service: DosService;
    credentialService: CredentialsService;
    customerService: CustomersService;
    companyService: CompaniesService;
    httpService: HttpService;
    constructor(service: DosService, credentialService: CredentialsService, customerService: CustomersService, companyService: CompaniesService, httpService: HttpService);
    getData(companies: CompanyEntity[], currentIndex: number): void;
    parseDoEntity(data: any): DoEntity;
    createOrUpdateCompany(company: CompanyEntity, listener: (company: CompanyEntity) => void): void;
    createOrUpdateCredential(credential: CredentialEntity, listener: (credential: CredentialEntity) => void): Promise<void>;
    createOrUpdateDOEntity(doEntity: DoEntity, listener: (doEntity: DoEntity) => void): void;
    createOrUpdateCustomer(customer: CustomerEntity, listener: (customer: CustomerEntity) => void): void;
}
