"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const dos_service_1 = require("./dos.service");
const dos_controller_1 = require("./dos.controller");
const typeorm_1 = require("@nestjs/typeorm");
const dos_entity_1 = require("./dos.entity");
const platform_express_1 = require("@nestjs/platform-express");
const medias_entity_1 = require("../medias/medias.entity");
const drivers_entity_1 = require("../drivers/drivers.entity");
const passport_1 = require("@nestjs/passport");
const jwt_1 = require("@nestjs/jwt");
const medias_service_1 = require("../medias/medias.service");
const do_transactions_service_1 = require("../do-transactions/do-transactions.service");
const do_transactions_entity_1 = require("../do-transactions/do-transactions.entity");
const customers_entity_1 = require("../customers/customers.entity");
const customers_service_1 = require("../customers/customers.service");
const credentials_service_1 = require("../credentials/credentials.service");
const credentials_entity_1 = require("../credentials/credentials.entity");
const companies_entity_1 = require("../companies/companies.entity");
const companies_service_1 = require("../companies/companies.service");
const blast_email_service_1 = require("../blast-email/blast-email.service");
const blast_email_entity_1 = require("../blast-email/blast-email.entity");
const dos_no_auth_controller_1 = require("./dos-no-auth.controller");
const dos_get_from_server_service_1 = require("./dos-get-from-server.service");
const multer_1 = require("multer");
const path_1 = require("path");
let DosModule = class DosModule {
};
DosModule = __decorate([
    common_1.Module({
        imports: [
            common_1.HttpModule,
            passport_1.PassportModule.register({ defaultStrategy: 'jwt' }),
            jwt_1.JwtModule.register({
                secretOrPrivateKey: 'secretKey',
                signOptions: {
                    expiresIn: 3600,
                },
            }),
            typeorm_1.TypeOrmModule.forFeature([
                dos_entity_1.DoEntity,
                do_transactions_entity_1.DoTransactionEntity,
                drivers_entity_1.DriverEntity,
                medias_entity_1.MediaEntity,
                customers_entity_1.CustomerEntity,
                credentials_entity_1.CredentialEntity,
                companies_entity_1.CompanyEntity,
                blast_email_entity_1.BlastEmailEntity,
            ]),
            platform_express_1.MulterModule.register({
                storage: multer_1.diskStorage({
                    destination: (req, file, cb) => {
                        cb(null, 'upload');
                    },
                    filename: (req, file, cb) => {
                        const randomName = Array(32)
                            .fill(null)
                            .map(() => Math.round(Math.random() * 16).toString(16))
                            .join('');
                        cb(null, `${randomName}${path_1.extname(file.originalname)}`);
                    },
                }),
            }),
        ],
        providers: [
            dos_service_1.DosService,
            dos_get_from_server_service_1.GetDoFromServerService,
            do_transactions_service_1.DoTransactionsService,
            medias_service_1.MediaService,
            customers_service_1.CustomersService,
            credentials_service_1.CredentialsService,
            companies_service_1.CompaniesService,
            blast_email_service_1.BlastEmailService,
        ],
        controllers: [dos_controller_1.DosController, dos_no_auth_controller_1.DosNoAuthController],
    })
], DosModule);
exports.DosModule = DosModule;
