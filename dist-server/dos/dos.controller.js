"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const dos_entity_1 = require("./dos.entity");
const crud_1 = require("@nestjsx/crud");
const dos_service_1 = require("./dos.service");
const passport_1 = require("@nestjs/passport");
const drivers_entity_1 = require("../drivers/drivers.entity");
const do_transactions_service_1 = require("../do-transactions/do-transactions.service");
const do_status_entity_1 = require("./do-status.entity");
const medias_entity_1 = require("../medias/medias.entity");
const medias_service_1 = require("../medias/medias.service");
const do_transactions_entity_1 = require("../do-transactions/do-transactions.entity");
const customers_entity_1 = require("../customers/customers.entity");
const admin = require("firebase-admin");
const acl_service_1 = require("../acl/acl.service");
const blast_email_service_1 = require("../blast-email/blast-email.service");
let DosController = class DosController {
    constructor(service, transactionService, mediaService, blastEmailService) {
        this.service = service;
        this.transactionService = transactionService;
        this.mediaService = mediaService;
        this.blastEmailService = blastEmailService;
    }
    get base() {
        return this;
    }
    getOne(req, parsedQuery, parsedOptions) {
        return this.base.getOneBase(parsedQuery, parsedOptions).then(next => {
            const transactionIds = next.transactions.map(t => {
                return { id: t.id };
            });
            if (transactionIds.length > 0) {
                const m = {
                    where: transactionIds,
                    join: {
                        alias: 'transaction',
                        leftJoinAndSelect: {
                            medias: 'transaction.medias',
                        },
                    },
                };
                return this.transactionService.find(m).then(transactions => {
                    next.transactions = transactions;
                    return next;
                });
            }
            else {
                return next;
            }
        });
    }
    getMany(req, parsedQuery, parsedOptions) {
        return __awaiter(this, void 0, void 0, function* () {
            if (parsedQuery.filter) {
                if (req.user) {
                    if (req.user instanceof drivers_entity_1.DriverEntity) {
                        if (parsedQuery.join.find(x => x.field == 'historyDrivers')) {
                            parsedQuery.filter.push({
                                field: 'historyDrivers.id',
                                operator: 'eq',
                                value: req.user.id,
                            });
                        }
                        else {
                            parsedQuery.filter.push({
                                field: 'currentDriver.id',
                                operator: 'eq',
                                value: req.user.id,
                            });
                            parsedQuery.join.push({
                                field: 'currentDriver',
                            });
                        }
                    }
                    else if (req.user instanceof customers_entity_1.CustomerEntity) {
                        parsedQuery.filter.push({
                            field: 'customer.id',
                            operator: 'eq',
                            value: req.user.id,
                        });
                        parsedQuery.join.push({
                            field: 'customer',
                        });
                    }
                }
            }
            let dataDos = yield this.base.getManyBase(parsedQuery, parsedOptions);
            if (!parsedQuery.page) {
                let data = this.removeDuplicates(dataDos, "noDO", "company");
                return new Promise((resolve, reject) => {
                    resolve(data);
                });
            }
            else {
                return new Promise((resolve, reject) => {
                    resolve(dataDos);
                });
            }
        });
    }
    removeDuplicates(array, key, key2) {
        let lookup = {};
        let result = [];
        var counter;
        counter = 0;
        var keyArray;
        array.forEach(element => {
            keyArray = String(element[key]) + "_" + element[key2];
            if ((!lookup[element[key] + "_" + element[key2]])) {
                lookup[element[key] + "_" + element[key2]] = true;
                result.push(element);
                counter = counter + 1;
            }
        });
        return result;
    }
    loadInDo(req, files, doId) {
        const driver = req.user;
        const medias = this.createMediaEntitiesFrom(files.loadInPhotos);
        return this.createDoTransactionWith(doId, driver, medias, do_status_entity_1.DoStatus.LoadIn);
    }
    sendDo(req, files, doId) {
        const driver = req.user;
        const medias = this.createMediaEntitiesFrom(files.letterPhotos);
        return this.createDoTransactionWith(doId, driver, medias, do_status_entity_1.DoStatus.Started).then(next => {
            return this.sendEmail('send', parseInt(doId)).then(email => {
                return next;
            }, error => {
                return next;
            });
        });
    }
    finishDo(req, files, doId) {
        const driver = req.user;
        const medias = this.createMediaEntitiesFrom(files.loadOutPhotos);
        const letterMedias = this.createMediaEntitiesFrom(files.letterPhotos);
        return this.createDoTransactionWith(doId, driver, letterMedias, do_status_entity_1.DoStatus.SignedDeliveryOrder).then(next => {
            return this.createDoTransactionWith(doId, driver, medias, do_status_entity_1.DoStatus.Finished).then(next => {
                return this.sendEmail('finish', parseInt(doId)).then(email => {
                    return next;
                }, error => {
                    return next;
                });
            });
        });
    }
    confirmedDo(req, doId, body) {
        return this.service.findOne({ id: parseInt(doId) }).then(deliveryOrder => {
            deliveryOrder.customerComment = body.customerComment;
            deliveryOrder.status = body.status;
            return this.service.updateOne(deliveryOrder).then(next => {
                return this.sendEmail('confirmed', parseInt(doId)).then(email => {
                    return next;
                }, error => {
                    return next;
                });
            });
        });
    }
    startDO(req, files, doId) {
        const driver = req.user;
        const loadInMedias = this.createMediaEntitiesFrom(files.loadInPhotos);
        const letterMedias = this.createMediaEntitiesFrom(files.letterPhotos);
        return this.createDoTransactionWith(doId, driver, loadInMedias, do_status_entity_1.DoStatus.LoadIn)
            .then(next => {
            return this.createDoTransactionWith(doId, driver, letterMedias, do_status_entity_1.DoStatus
                .Started);
        });
    }
    getDriverActiveDo(req) {
        const driver = req.user;
        const filter = {
            where: {
                currentDriver: driver,
                status: do_status_entity_1.DoStatus.Started,
            },
            relations: ['transactions', 'currentDriver'],
        };
        return this.service.findOne(filter).then(activeDo => {
            return activeDo ? activeDo : {};
        });
    }
    updateTransactionWith(doId, driver, files, status) {
        return this.service.findOne({
            where: {
                id: parseInt(doId),
                currentDriver: driver,
            },
            relations: ['transactions']
        }).then(deliveryOrder => {
            if (!deliveryOrder) {
                throw new common_1.BadRequestException();
            }
            return this.mediaService.createMany({
                bulk: files,
            }).then(next => {
                const transaction = deliveryOrder.transactions.find(x => x.status == status);
                return this.transactionService.findOne(transaction.id, { relations: ['medias'] }).then(transaction => {
                    next.forEach(file => transaction.medias.push(file));
                    return this.transactionService.updateOne(transaction, []);
                });
            });
        });
    }
    createDoTransactionWith(doId, driver, files, status) {
        return this.service.findOne({
            where: {
                id: parseInt(doId),
                currentDriver: driver,
            },
            relations: ['customer'],
        }).then((deliveryOrder) => __awaiter(this, void 0, void 0, function* () {
            if (!deliveryOrder) {
                throw new common_1.BadRequestException();
            }
            let dataOrder = yield this.service.find({
                where: {
                    noDO: deliveryOrder.noDO,
                    company: deliveryOrder.company
                }
            });
            dataOrder.forEach(itemDeliveryOrder => {
                if (itemDeliveryOrder.customer && itemDeliveryOrder.customer.firebaseToken) {
                    this.sendDoStatusNotification(itemDeliveryOrder.customer, status, itemDeliveryOrder);
                }
                itemDeliveryOrder.status = status;
                itemDeliveryOrder.updatedDate = new Date();
                this.service.updateOne(itemDeliveryOrder).then(savedDeliveryOrdeer => {
                    this.mediaService.createMany({
                        bulk: files,
                    }).then(next => {
                        const transaction = new do_transactions_entity_1.DoTransactionEntity();
                        transaction.assignedDriver = driver;
                        transaction.status = status;
                        transaction.do = savedDeliveryOrdeer;
                        transaction.medias = next;
                        this.transactionService.createOne(transaction, [])
                            .then(savedTransaction => {
                            itemDeliveryOrder.currentTransactionId = savedTransaction.id;
                            this.service.updateOne(itemDeliveryOrder);
                        });
                    });
                });
            });
            return this.service.updateOne(dataOrder[0]).then(savedDeliveryOrdeer => {
                return this.mediaService.createMany({
                    bulk: files,
                }).then(next => {
                    const transaction = new do_transactions_entity_1.DoTransactionEntity();
                    transaction.assignedDriver = driver;
                    transaction.status = status;
                    transaction.do = savedDeliveryOrdeer;
                    transaction.medias = next;
                    return this.transactionService.createOne(transaction, [])
                        .then(savedTransaction => {
                        dataOrder[0].currentTransactionId = savedTransaction.id;
                        return this.service.updateOne(dataOrder[0]);
                    });
                });
            });
        }));
    }
    sendDoStatusNotification(customer, doStatus, deliveryOrder) {
        let title = '';
        switch (doStatus) {
            case do_status_entity_1.DoStatus.LoadIn:
                title = 'Pesanan anda (' + deliveryOrder.noDO + ') sudah di load in';
                break;
            case do_status_entity_1.DoStatus.Started:
                title = 'Pesanan anda (' + deliveryOrder.noDO + ') sedang dikirim';
                break;
            case do_status_entity_1.DoStatus.Finished:
                title = 'Pesanan anda (' + deliveryOrder.noDO + ') sudah sampai di tujuan';
                break;
        }
        this.sendNotification(customer.firebaseToken, title, '');
    }
    sendNotification(token, title, body) {
        if (token == null || token == undefined || token == '') {
            return;
        }
        admin.messaging().sendToDevice(token, {
            data: {},
            notification: {
                title,
                body
            },
        }).then(success => {
            success.results.forEach(result => {
                console.log(result);
            });
            console.log(success);
        }, error => {
            console.log(error);
        });
    }
    createMediaEntitiesFrom(files) {
        return files.map(file => {
            const media = new medias_entity_1.MediaEntity();
            media.url = file.path.replace('\\', '/');
            return media;
        });
    }
    sendEmail(actionCode, doId) {
        return this.blastEmailService.sendMailToDo(doId, actionCode);
    }
};
__decorate([
    crud_1.Override(),
    __param(0, common_1.Req()), __param(1, crud_1.ParsedQuery()), __param(2, crud_1.ParsedOptions()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, crud_1.RestfulParamsDto, Object]),
    __metadata("design:returntype", void 0)
], DosController.prototype, "getOne", null);
__decorate([
    crud_1.Override(),
    __param(0, common_1.Req()), __param(1, crud_1.ParsedQuery()), __param(2, crud_1.ParsedOptions()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, crud_1.RestfulParamsDto, Object]),
    __metadata("design:returntype", Promise)
], DosController.prototype, "getMany", null);
__decorate([
    common_1.Post('loadIn'),
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.UseInterceptors(platform_express_1.FileFieldsInterceptor([{
            name: 'loadInPhotos',
        }])),
    __param(0, common_1.Req()), __param(1, common_1.UploadedFiles()), __param(2, common_1.Query('doId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String]),
    __metadata("design:returntype", void 0)
], DosController.prototype, "loadInDo", null);
__decorate([
    common_1.Post('send'),
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.UseInterceptors(platform_express_1.FileFieldsInterceptor([{
            name: 'letterPhotos',
        }])),
    __param(0, common_1.Req()), __param(1, common_1.UploadedFiles()), __param(2, common_1.Query('doId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String]),
    __metadata("design:returntype", void 0)
], DosController.prototype, "sendDo", null);
__decorate([
    common_1.Post('finish'),
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.UseInterceptors(platform_express_1.FileFieldsInterceptor([{
            name: 'loadOutPhotos',
        }, {
            name: 'letterPhotos',
        }])),
    __param(0, common_1.Req()), __param(1, common_1.UploadedFiles()), __param(2, common_1.Query('doId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String]),
    __metadata("design:returntype", void 0)
], DosController.prototype, "finishDo", null);
__decorate([
    common_1.Post('confirmed'),
    common_1.UseGuards(passport_1.AuthGuard()),
    __param(0, common_1.Req()), __param(1, common_1.Query('doId')), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, Object]),
    __metadata("design:returntype", void 0)
], DosController.prototype, "confirmedDo", null);
__decorate([
    common_1.Post('start'),
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.UseInterceptors(platform_express_1.FileFieldsInterceptor([{
            name: 'loadInPhotos',
        },
        {
            name: 'letterPhotos',
        },
    ])),
    __param(0, common_1.Req()), __param(1, common_1.UploadedFiles()), __param(2, common_1.Query('doId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, String]),
    __metadata("design:returntype", void 0)
], DosController.prototype, "startDO", null);
__decorate([
    common_1.Get('activeDo'),
    common_1.UseGuards(passport_1.AuthGuard()),
    __param(0, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], DosController.prototype, "getDriverActiveDo", null);
DosController = __decorate([
    common_1.UseGuards(passport_1.AuthGuard(), acl_service_1.ACLGuard),
    crud_1.Feature('Dos'),
    crud_1.Crud(dos_entity_1.DoEntity, {
        options: {
            join: {
                transactions: {
                    allow: [],
                },
                currentDriver: {
                    allow: [],
                },
                locations: {
                    allow: [],
                },
                customer: {
                    allow: [],
                },
                vehicle: {
                    allow: [],
                },
                historyDrivers: {
                    allow: []
                }
            },
        },
    }),
    common_1.Controller('dos'),
    __metadata("design:paramtypes", [dos_service_1.DosService,
        do_transactions_service_1.DoTransactionsService,
        medias_service_1.MediaService,
        blast_email_service_1.BlastEmailService])
], DosController);
exports.DosController = DosController;
