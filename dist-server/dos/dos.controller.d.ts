import { DoEntity } from './dos.entity';
import { CrudController, RestfulParamsDto, CrudOptions } from '@nestjsx/crud';
import { DosService } from './dos.service';
import { DoTransactionsService } from '../do-transactions/do-transactions.service';
import { MediaService } from '../medias/medias.service';
import { BlastEmailService } from '../blast-email/blast-email.service';
export declare class DosController {
    service: DosService;
    private transactionService;
    private mediaService;
    private blastEmailService;
    constructor(service: DosService, transactionService: DoTransactionsService, mediaService: MediaService, blastEmailService: BlastEmailService);
    readonly base: CrudController<DosService, DoEntity>;
    getOne(req: any, parsedQuery: RestfulParamsDto, parsedOptions: CrudOptions): Promise<DoEntity>;
    getMany(req: any, parsedQuery: RestfulParamsDto, parsedOptions: CrudOptions): Promise<{}>;
    private removeDuplicates;
    loadInDo(req: any, files: any, doId: string): Promise<DoEntity>;
    sendDo(req: any, files: any, doId: string): Promise<DoEntity>;
    finishDo(req: any, files: any, doId: string): Promise<DoEntity>;
    confirmedDo(req: any, doId: string, body: any): Promise<DoEntity>;
    startDO(req: any, files: any, doId: string): Promise<DoEntity>;
    getDriverActiveDo(req: any): Promise<{}>;
    private updateTransactionWith;
    private createDoTransactionWith;
    private sendDoStatusNotification;
    private sendNotification;
    private createMediaEntitiesFrom;
    private sendEmail;
}
