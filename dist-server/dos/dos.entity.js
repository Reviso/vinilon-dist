"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const do_transactions_entity_1 = require("../do-transactions/do-transactions.entity");
const swagger_1 = require("@nestjs/swagger");
const drivers_entity_1 = require("../drivers/drivers.entity");
const locations_entity_1 = require("../locations/locations.entity");
const customers_entity_1 = require("../customers/customers.entity");
const vehicles_entity_1 = require("../vehicles/vehicles.entity");
let DoEntity = class DoEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], DoEntity.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "company", void 0);
__decorate([
    typeorm_1.Column({
        unique: false,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "noDO", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Date)
], DoEntity.prototype, "date", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
        onUpdate: 'CURRENT_TIMESTAMP',
        type: 'timestamp',
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Date)
], DoEntity.prototype, "updatedDate", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "noSO", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "customerCode", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "customerName", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "address1", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "address2", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "address3", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "phone", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "noPO", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "itemName", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], DoEntity.prototype, "qty", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "uom", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], DoEntity.prototype, "weight", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "note1", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "note2", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "spm", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "spk", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "pickUp", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "customerEmail", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "collectionEmail", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "salesEmail", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "deliveryEmail", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
        default: 'New',
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "status", void 0);
__decorate([
    typeorm_1.Column({
        type: 'decimal',
        precision: 12,
        scale: 9,
        default: 0,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], DoEntity.prototype, "lat", void 0);
__decorate([
    typeorm_1.Column({
        type: 'decimal',
        precision: 12,
        scale: 9,
        default: 0,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], DoEntity.prototype, "lng", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "deviceId", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], DoEntity.prototype, "customerComment", void 0);
__decorate([
    typeorm_1.ManyToOne(type => customers_entity_1.CustomerEntity, object => object.dos, {
        onDelete: 'SET NULL',
    }),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", customers_entity_1.CustomerEntity)
], DoEntity.prototype, "customer", void 0);
__decorate([
    typeorm_1.OneToMany(type => do_transactions_entity_1.DoTransactionEntity, transaction => transaction.do),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Array)
], DoEntity.prototype, "transactions", void 0);
__decorate([
    typeorm_1.ManyToOne(type => drivers_entity_1.DriverEntity, driver => driver.dos, {
        onDelete: 'SET NULL',
    }),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", drivers_entity_1.DriverEntity)
], DoEntity.prototype, "currentDriver", void 0);
__decorate([
    typeorm_1.ManyToMany(type => drivers_entity_1.DriverEntity, driver => driver.allDos),
    typeorm_1.JoinTable(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Array)
], DoEntity.prototype, "historyDrivers", void 0);
__decorate([
    typeorm_1.ManyToOne(type => vehicles_entity_1.VehicleEntity, object => object.dos, {
        onDelete: 'SET NULL',
    }),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", vehicles_entity_1.VehicleEntity)
], DoEntity.prototype, "vehicle", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], DoEntity.prototype, "currentTransactionId", void 0);
__decorate([
    typeorm_1.OneToMany(type => locations_entity_1.LocationsEntity, object => object.do),
    typeorm_1.JoinColumn(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Array)
], DoEntity.prototype, "locations", void 0);
DoEntity = __decorate([
    typeorm_1.Entity()
], DoEntity);
exports.DoEntity = DoEntity;
