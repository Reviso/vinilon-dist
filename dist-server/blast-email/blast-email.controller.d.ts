import { BlastEmailService } from './blast-email.service';
export declare class BlastEmailController {
    service: BlastEmailService;
    constructor(service: BlastEmailService);
    createIfThereIsNo(action: string, actionCode: string): void;
}
