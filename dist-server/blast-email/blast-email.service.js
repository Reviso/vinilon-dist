"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjsx/crud/typeorm");
const blast_email_entity_1 = require("./blast-email.entity");
const typeorm_2 = require("@nestjs/typeorm");
const nodemailer = require("nodemailer");
const dos_service_1 = require("../dos/dos.service");
const config_1 = require("../config");
const dateformat = require("dateformat");
let BlastEmailService = class BlastEmailService extends typeorm_1.RepositoryService {
    constructor(repo, doService) {
        super(repo);
        this.doService = doService;
    }
    sendMailToDo(doId, actionCode) {
        return this.doService.findOne(doId).then(deliveryOrder => {
            this.repo
                .findOne({
                where: {
                    actionCode,
                },
            })
                .then((email) => __awaiter(this, void 0, void 0, function* () {
                const receivers = [];
                if (email.sales) {
                    receivers.push(deliveryOrder.salesEmail);
                }
                if (email.customer) {
                    receivers.push(deliveryOrder.customerEmail);
                }
                if (email.collection) {
                    receivers.push(deliveryOrder.collectionEmail);
                }
                if (email.delivery) {
                    receivers.push(deliveryOrder.deliveryEmail);
                }
                let template = email.template;
                let loopingData = this.GetBetweenTag("@loop@", "@/loop@", template);
                if (!this.isEmpty(loopingData)) {
                    var resultLooping = yield this.GetMultiSetDataString(deliveryOrder.noDO, loopingData);
                }
                template = template.replace(loopingData, resultLooping);
                template = template.replace('@{COMPANY}', deliveryOrder.company);
                template = template.replace('@{NO_DO}', deliveryOrder.noDO);
                const formattedDeliveryDate = dateformat(deliveryOrder.date, 'dd - mm - yyyy');
                template = template.replace('@{TGL_DO}', formattedDeliveryDate);
                template = template.replace('@{NO_SO}', deliveryOrder.noSO);
                template = template.replace('@{KODE_CUSTOMER}', deliveryOrder.customerCode);
                template = template.replace('@{NAMA_CUSTOMER}', deliveryOrder.customerName);
                template = template.replace('@{ADD_1}', deliveryOrder.address1);
                template = template.replace('@{ADD_2}', deliveryOrder.address2);
                template = template.replace('@{ADD_3}', deliveryOrder.address3);
                template = template.replace('@{PHONE}', deliveryOrder.phone);
                template = template.replace('@{NO_PO}', deliveryOrder.noPO);
                template = template.replace('@{NAMA_BARANG}', deliveryOrder.itemName + '');
                template = template.replace('@{QTY}', deliveryOrder.qty + '');
                template = template.replace('@{UOM}', deliveryOrder.uom);
                template = template.replace('@{BERAT_BARANG}', deliveryOrder.weight + '');
                template = template.replace('@{KETERANGAN_1}', deliveryOrder.note1);
                template = template.replace('@{KETERANGAN_2}', deliveryOrder.note2);
                template = template.replace('@{SPM}', deliveryOrder.spm);
                template = template.replace('@{EMAIL_COLLECTION}', deliveryOrder.collectionEmail);
                template = template.replace('@{EMAIL_CUSTOMER}', deliveryOrder.customerEmail);
                template = template.replace('@{EMAIL_SALES}', deliveryOrder.salesEmail);
                template = template.replace('@{EMAIL_DELIVERY}', deliveryOrder.deliveryEmail);
                template = template.replace('@{SPK}', deliveryOrder.spk);
                template = template.replace('@{PICK_UP}', deliveryOrder.pickUp);
                let subject = email.subject;
                subject = subject.replace('@{COMPANY}', deliveryOrder.company);
                subject = subject.replace('@{NO_DO}', deliveryOrder.noDO);
                subject = subject.replace('@{TGL_DO}', formattedDeliveryDate);
                subject = subject.replace('@{NO_SO}', deliveryOrder.noSO);
                subject = subject.replace('@{KODE_CUSTOMER}', deliveryOrder.customerCode);
                subject = subject.replace('@{NAMA_CUSTOMER}', deliveryOrder.customerName);
                subject = subject.replace('@{ADD_1}', deliveryOrder.address1);
                subject = subject.replace('@{ADD_2}', deliveryOrder.address2);
                subject = subject.replace('@{ADD_3}', deliveryOrder.address3);
                subject = subject.replace('@{PHONE}', deliveryOrder.phone);
                subject = subject.replace('@{NO_PO}', deliveryOrder.noPO);
                subject = subject.replace('@{NAMA_BARANG}', deliveryOrder.itemName);
                subject = subject.replace('@{QTY}', deliveryOrder.qty + '');
                subject = subject.replace('@{UOM}', deliveryOrder.uom);
                subject = subject.replace('@{BERAT_BARANG}', deliveryOrder.weight + '');
                subject = subject.replace('@{KETERANGAN_1}', deliveryOrder.note1);
                subject = subject.replace('@{KETERANGAN_2}', deliveryOrder.note2);
                subject = subject.replace('@{SPM}', deliveryOrder.spm);
                subject = subject.replace('@{EMAIL_COLLECTION}', deliveryOrder.collectionEmail);
                subject = subject.replace('@{EMAIL_CUSTOMER}', deliveryOrder.customerEmail);
                subject = subject.replace('@{EMAIL_SALES}', deliveryOrder.salesEmail);
                subject = subject.replace('@{EMAIL_DELIVERY}', deliveryOrder.deliveryEmail);
                subject = subject.replace('@{SPK}', deliveryOrder.spk);
                subject = subject.replace('@{PICK_UP}', deliveryOrder.pickUp);
                const receiver = receivers.join(',');
                return this.sendMail(receiver, subject, template);
            }));
        });
    }
    sendMail(to, subject, html) {
        const transporter = nodemailer.createTransport({
            host: config_1.config.smtpHost,
            port: config_1.config.smtpPort,
            secure: true,
            auth: {
                user: config_1.config.smtpUsername,
                pass: config_1.config.smtpPassword,
            },
        });
        const mailOptions = {
            from: 'noreply@vinilon.com',
            to,
            subject,
            html,
        };
        return new Promise((resolve, reject) => {
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                else {
                    console.log('Email sent: ' + info.response);
                    resolve(info.response);
                }
            });
        });
    }
    GetMultiSetDataString(DONumber, template) {
        return __awaiter(this, void 0, void 0, function* () {
            template = template.replace("@loop@", "").replace("@/loop@", "");
            let result = "";
            let resultTemplate = "";
            let dataDelivery = yield this.doService.find({
                where: {
                    noDO: DONumber
                }
            });
            dataDelivery.forEach(deliveryOrder => {
                result = template;
                result = result.replace("@{COMPANY}", deliveryOrder.company);
                result = result.replace("@{NO_DO}", deliveryOrder.noDO);
                const formattedDeliveryDate = dateformat(deliveryOrder.date, "dd - mm - yyyy");
                result = result.replace("@{TGL_DO}", formattedDeliveryDate);
                result = result.replace("@{NO_SO}", deliveryOrder.noSO);
                result = result.replace("@{KODE_CUSTOMER}", deliveryOrder.customerCode);
                result = result.replace("@{NAMA_CUSTOMER}", deliveryOrder.customerName);
                result = result.replace("@{ADD_1}", deliveryOrder.address1);
                result = result.replace("@{ADD_2}", deliveryOrder.address2);
                result = result.replace("@{ADD_3}", deliveryOrder.address3);
                result = result.replace("@{PHONE}", deliveryOrder.phone);
                result = result.replace("@{NO_PO}", deliveryOrder.noPO);
                result = result.replace("@{NAMA_BARANG}", deliveryOrder.itemName + "");
                result = result.replace("@{QTY}", deliveryOrder.qty + "");
                result = result.replace("@{UOM}", deliveryOrder.uom);
                result = result.replace("@{BERAT_BARANG}", deliveryOrder.weight + "");
                result = result.replace("@{KETERANGAN_1}", deliveryOrder.note1);
                result = result.replace("@{KETERANGAN_2}", deliveryOrder.note2);
                result = result.replace("@{SPM}", deliveryOrder.spm);
                result = result.replace("@{EMAIL_COLLECTION}", deliveryOrder.collectionEmail);
                result = result.replace("@{EMAIL_CUSTOMER}", deliveryOrder.customerEmail);
                result = result.replace("@{EMAIL_SALES}", deliveryOrder.salesEmail);
                result = result.replace("@{EMAIL_DELIVERY}", deliveryOrder.deliveryEmail);
                result = result.replace("@{SPK}", deliveryOrder.spk);
                result = result.replace("@{PICK_UP}", deliveryOrder.pickUp);
                resultTemplate = resultTemplate + result;
            });
            return resultTemplate;
        });
    }
    GetBetweenTag(firstCharacter, secondCharacter, template) {
        let regExString = new RegExp("(?:" + firstCharacter + ")(.*?)(?:" + secondCharacter + ")", "ig");
        let testRE = regExString.exec(template);
        if (testRE && testRE.length > 1) {
            return testRE[0];
        }
        else {
            return "";
        }
    }
    isEmpty(str) {
        return (!str || 0 === str.length);
    }
};
BlastEmailService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(blast_email_entity_1.BlastEmailEntity)),
    __metadata("design:paramtypes", [Object, dos_service_1.DosService])
], BlastEmailService);
exports.BlastEmailService = BlastEmailService;
