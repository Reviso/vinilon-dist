"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const users_module_1 = require("./users/users.module");
const companies_module_1 = require("./companies/companies.module");
const problems_module_1 = require("./problems/problems.module");
const vehicles_module_1 = require("./vehicles/vehicles.module");
const dos_module_1 = require("./dos/dos.module");
const drivers_module_1 = require("./drivers/drivers.module");
const credentials_module_1 = require("./credentials/credentials.module");
const customers_module_1 = require("./customers/customers.module");
const auth_module_1 = require("./auth/auth.module");
const do_transactions_module_1 = require("./do-transactions/do-transactions.module");
const medias_module_1 = require("./medias/medias.module");
const locations_module_1 = require("./locations/locations.module");
const do_mutations_module_1 = require("./do-mutations/do-mutations.module");
const acl_service_1 = require("./acl/acl.service");
const core_1 = require("@nestjs/core");
const acls_module_1 = require("./acls/acls.module");
const blast_email_module_1 = require("./blast-email/blast-email.module");
const modules_module_1 = require("./modules/modules.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot(),
            users_module_1.UsersModule,
            companies_module_1.CompaniesModule,
            problems_module_1.ProblemsModule,
            vehicles_module_1.VehiclesModule,
            dos_module_1.DosModule,
            do_transactions_module_1.DoTransactionsModule,
            medias_module_1.MediaModule,
            drivers_module_1.DriversModule,
            customers_module_1.CustomersModule,
            credentials_module_1.CredentialsModule,
            auth_module_1.AuthModule,
            locations_module_1.LocationsModule,
            do_mutations_module_1.DoMutationsModule,
            acls_module_1.AclsModule,
            blast_email_module_1.BlastEmailModule,
            modules_module_1.ModulesModule,
            common_1.HttpModule,
            users_module_1.UsersModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService, { provide: core_1.APP_GUARD, useClass: acl_service_1.ACLGuard }],
    })
], AppModule);
exports.AppModule = AppModule;
